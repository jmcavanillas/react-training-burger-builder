import axios from 'axios';

const orderStore = axios.create({
    baseURL: 'https://burger-builder-ade1c.firebaseio.com/'
});

export default orderStore;