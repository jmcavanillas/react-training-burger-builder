import React, { Component } from "react";
import { connect } from 'react-redux';

import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import axios from '../../axios-orders';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../containers/withErrorHandler/withErrorHandler';

import * as actions from '../../store/actions/index';

class BurguerBuilder extends Component
{
    state = {
        purchasing: false
    }

    updatePurchaseState = (ingredients) => {
        const sum = Object.keys(ingredients)
            .map(igKey => {
                return ingredients[igKey];
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0);

            return sum > 0;
    }

    purchase = () => {
        if(this.props.isAuth)
            this.setState({purchasing: true});
            else
            {
                this.props.onSetAuthRedirectPath('/checkout');
                this.props.history.push('/auth');
            }
    }

    cancel = () => {
        this.setState({purchasing: false});
    }

    continuePurchase = () => {
        this.props.onInitPurchase();
        this.props.history.push('/checkout');
    }

    componentDidMount = () => {
        this.props.onInitIngredients();
    }

    render() 
    {
        const disabledInfo = {
            ...this.props.ings
        };
        
        for(let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0;
        }
        let orderSummary = null;

        let burger =  this.props.error ? <p>Ingredients can't be loaded</p> : <Spinner />;

        if(this.props.ings )
        {
            orderSummary = <OrderSummary ingredients={this.props.ings}
            price={this.props.price.toFixed(2)} 
            cancel={this.cancel} accept={this.continuePurchase}/>;
            burger = (
                <React.Fragment>    
                    <Burger ingredients={this.props.ings} />
                    <BuildControls 
                        isAuth={this.props.isAuth}
                        ingredientAdded={this.props.onIngredientAdded}
                        ingredientRemoved={this.props.onIngredientRemoved}
                        disabled={disabledInfo}
                        purchasable={this.updatePurchaseState(this.props.ings)}
                        price={this.props.price}
                        ordered={this.purchase} />
                </React.Fragment>
            );
        }

        return (
            <React.Fragment>
                <Modal show={this.state.purchasing} modalClosed={this.cancel}>
                    {orderSummary}
                </Modal>
                {burger}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        error: state.burgerBuilder.error,
        isAuth: state.auth.token !== null
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onIngredientAdded: ing => dispatch(actions.addIngredient(ing)),
        onIngredientRemoved: ing => dispatch(actions.removeIngredient(ing)),
        onInitIngredients: () => dispatch(actions.initIngredients()),
        onInitPurchase: () => dispatch(actions.purchaseInit()), 
        onSetAuthRedirectPath: path => dispatch(actions.setAuthRedirectPath(path))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurguerBuilder, axios));