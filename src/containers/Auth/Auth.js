import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/Spinner/Spinner';
import classes from './Auth.module.css';

import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

class Auth extends Component {
    state = {
        controls: {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Mail Address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true,
                    valid: false
                },
                pristine: true
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Your Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6,
                    valid: false
                },
                pristine: true
            }
        },
        isSignUp: true
    }

    validate(value, rules) {
        let isValid = true;

        if (!rules) return;

        if (rules.required && isValid) {
            isValid = value.trim() !== '';
        }

        if (rules.minLength && isValid) {
            isValid = value.length >= rules.minLength;
        }

        if (rules.maxLength && isValid) {
            isValid = value.length <= rules.maxLength;
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangeHandler = (controlName, event) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                validation: {
                    ...this.state.controls[controlName].validation,
                    valid: this.validate(event.target.value, this.state.controls[controlName].validation)
                },
                pristine: false
            }
        };

        this.setState({controls: updatedControls});
    }

    submitHandler = (event) => {
        event.preventDefault();
        this.props.onAuth(
            this.state.controls.email.value, 
            this.state.controls.password.value, 
            this.state.isSignUp);

    }

    switchAuthModeHandler = () => {
        this.setState(prevState => {
            return {isSignUp: !prevState.isSignUp};
        });
    }

    componentDidMount () {
        if (!this.props.buildingBurger && this.props.authRedirectPath !== '/')
        {
            this.props.onSetAuthRedirectPath();
        }
    }

    render () {
        let authRedirect = null;
        if (this.props.isAuth)
            authRedirect = <Redirect to={this.props.authRedirectPath}/>;

        const formElementArray = [];

        for(let k in this.state.controls) {
            formElementArray.push ({
                id: k,
                config: this.state.controls[k]
            });
        }

        let form = formElementArray.map(formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType} 
                elementConfig={formElement.config.elementConfig} 
                value={formElement.config.value}
                invalid={!formElement.config.validation.valid}
                isPristine={formElement.config.pristine}
                changed={this.inputChangeHandler.bind(this, formElement.id)} />
        ));

        if (this.props.loading) 
            form = <Spinner />;

        let errorMessage = null;
        if(this.props.error)
            errorMessage = (
                <p>{this.props.error.message}</p>
            );

        return (
            <div className={classes.Auth}>
                {authRedirect}
                {errorMessage}
                <form onSubmit={this.submitHandler}>
                    {form}
                    <Button btnType="Success">Submit</Button>
                </form>
                <Button 
                    clicked={this.switchAuthModeHandler}
                    btnType="Danger">{this.state.isSignUp ? "Already have an Acount? Sign In" : "Don't have an Acount? Sign Up"}</Button>
            </div>
        );

    };
};

const mapStateToProps = state => {
    return {
        loading: state.auth.loading, 
        error: state.auth.error,
        isAuth: state.auth.token !== null,
        buildingBurger: state.burgerBuilder.building,
        authRedirectPath: state.auth.authRedirectPath
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (email, password, isSignUp) => dispatch(actions.auth(email, password, isSignUp)),
        onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/'))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Auth);