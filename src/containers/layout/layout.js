import React, { Component } from 'react';
import { connect } from 'react-redux';

import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';

class layout extends Component
{
    state = {
        showSideDrawer: false
    }

    close = () => {
        this.setState({showSideDrawer:false});
    }

    open = () => {
        this.setState({showSideDrawer:true});
    }

    render() 
    {
        return (
            <div>
            <Toolbar isAuth={this.props.isAuthenticated} openSideDrawer={this.open}/>
            <SideDrawer opened={this.state.showSideDrawer} close={this.close} />
            <main style={{marginTop: '72px'}}>
                
                <div className="container">
                { this.props.children }
                </div>
            </main>
            </div>
        )
    
    } 
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    };
};

export default connect(mapStateToProps)(layout);