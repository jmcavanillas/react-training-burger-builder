import React from 'react';
import BuildControl from './BuildControl/BuildControl';


const controls = [
    { label:'Salad', type:'salad' },
    { label:'Bacon', type:'bacon' },
    { label:'Cheese', type:'cheese' },
    { label:'Meat', type:'meat' },
];

const buildControls = ( props ) => (
    <div>
        <p>Current price: <strong>{props.price.toFixed(2)}</strong></p>
        { controls.map(ctrl => (
            <BuildControl 
            key={ctrl.label} 
            label={ctrl.label}
            added={props.ingredientAdded.bind(this, ctrl.type)}
            removed={props.ingredientRemoved.bind(this, ctrl.type)}
            disabled={props.disabled[ctrl.type]} />
        ))}
        <button
        disabled={!props.purchasable}
        onClick={props.ordered}>{props.isAuth ? 'Order Now' : 'Sign Up to order'}</button>
    </div>
);

export default buildControls;