import React from 'react';

import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import Backdrop from '../../UI/Modal/Backdrop/Backdrop';
import classes from './SideDrawer.module.css';


const sideDrawer = (props) => {
    let attachedClasses = [classes.SideDrawer, classes.Close];

    if (props.opened)
        attachedClasses = [classes.SideDrawer, classes.Open];

    return (
        <React.Fragment>
            <Backdrop show={props.opened} action={props.close} />
            <div className={attachedClasses.join(' ')}>
                <div className={classes.Logo}>
                    <Logo />
                </div>
                <nav>
                    <NavigationItems isAuth={props.isAuth}/>
                </nav>
            </div>
        </React.Fragment>
    );
}

export default sideDrawer;